package controle;

import modelo.dominio.No;
import modelo.dominio.Usuario;

/**
 *
 * @author Jefferson
 */
public class EstruturasDeDados{
    
    No x = new No();
    Usuario y = new Usuario();
    
    public No inicioLista;
    public No fimLista;
    public int qtdLista ;
    public int c, p;
    private String n;
    
    //retornar quantos espaços estão preenchido
    public int getQtd(){
        return qtdLista;
    }
    // setar quantos elementos vou ter
    public void setQtdLista(int n){
        qtdLista = n;
    }
   
   //verificar se é vazio
    public boolean isEmpty(){
       if (getQtd()== 0)
           return true;
       else
           return false;
           
   }
    //setar o inicio da lista
    public void setIL(No x){
         inicioLista = x;
    }
    //setar o fim da lista
    public void setFL(No y){
        fimLista = y;
    }
    //puxar o inicio da lista
    public No getIL(){
        return inicioLista;
    }
    public No getFL(){
        return fimLista;
    }
    
    public No getProx(){
        
         return this.getIL().getProximo();
        
    }
    
    public int getPosicao(){
        return p;
    }
        
    
    
    public No getLista(EstruturasDeDados listaE){
        
        if (!listaE.isEmpty(listaE)){
            return listaE.getIL();
        }
        
        return null;
        
    }
    
    public boolean Inserir(){
        if ((p<0)|| p>this.getQtd())
            return false;
        //caso 1 ( lista vazia )
        if(this.isEmpty()) {
            
            
                Usuario u = new Usuario(c,n);
                No novoNo = new No();
                novoNo.setProximo(null);
                novoNo.setObjeto(u);
                this.setIL(novoNo);
                this.setFL(novoNo);
                
            }
        //( caso 2, 1 elemento na lista )
        else if ((this.getQtd() == 1) && (p==1)){
                Usuario u = new Usuario(c,n);
                No nN = new No();
                nN.setProximo(null);
                this.getProx().setProximo(nN);
                this.setFL(nN);
        }
            //( caso 3, primeiro da lista )
            else if(p==0){
                Usuario u = new Usuario(c,n);
                No nN = new No();
                nN.setProximo(null);
                nN.setObjeto(u);
                nN.setProximo(this.getIL());
                this.setIL(nN);
            }
                
                //( caso 4, final da lista )
                else if(p == this.getQtd()-1){
                    Usuario u = new Usuario(c,n);
                    No nN = new No();
                    nN.setProximo(null);
                        No pAux;
                        pAux = this.getIL();
                        int i = 0;
                            while((pAux!=null) || (i<1)){
                                pAux = pAux.getProximo();
                            i++;
                            }
                        pAux.setProximo(nN);
                        nN.setProximo(this.getFL());
                        }    
                
                
            }
            
        }
    }

    
    
}
