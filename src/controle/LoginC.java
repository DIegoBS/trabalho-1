package controle;

import java.io.FileReader;
import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JOptionPane;

/**
 *
 * @author Jefferson
 */
public class LoginC {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private static FileReader fr;      // fr = FileReader
    private static BufferedReader br;  // br = BufferedReader
    private static String[] dados;
            
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    public LoginC(){
        
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método que recebe os dados de autenticação
    public static int autenticarUsuario(String _l, String _s){
        
        // Verifica se os campos login e senha da interface são campos vazios.
        if (!"".equals(_l) && !"".equals(_s))
            return autenticarDados(_l,_s);
            
        return 0;
        
    }
    
    // Método que vai autenticar o usuário de acordo com o arquivo 'senhas.txt'
    private static int autenticarDados(String _l, String _s){
        
        String caminhoDoArquivo = "C:\\Users\\Jefferson\\Documents\\NetBeansProjects\\SistemaLC\\src\\arquivos\\senhas.txt";
        String linha;
                
        try{
            
            // Acessando os atributos privados através do nome da Classe LoginC
            // por serem static
            LoginC.fr = new FileReader(caminhoDoArquivo); // Abrindo o arquivo 'senhas.txt'.
            LoginC.br = new BufferedReader(fr);           // Colocando o arquivo 'senhas.txt' em Buffer
            
            // Capturando a primeira linha do arquivo 'senhas.txt'.
            linha = br.readLine();
            
            // Atributo responsável por pegar os dados de cada linha do arquivo após a leitura.
            LoginC.dados = new String[2];
            
            // Enquanto existirem linhas no arquivo, executa o laço de repetição, ou seja, != null.
            while(linha != null){
                
                // Como os dados estão separados por espaço,
                // Vamos dividir a linha do arquivo em 2 partes (só existem 2 partes)
                // e enviar ao vetor dados.
                dados = linha.split(" ");
                
                // dados[0] possui o login (presente no arquivo 'senhas.txt')
                // dados[1] possui a senha (presente no arquivo 'senhas.txt')
                //
                // Compara os dados com os parâmetros do método
                if(dados[0].equals(_l) && dados[1].equals(_s))
                    return 1;
                
                // Caso não sejam iguais, ir para a próxima linha do arquivo.
                linha = br.readLine();
                
            }
            
         // Caso o java não consiga abrir o arquivo 'senhas.txt',
         // gera uma exceção que é tratada abaixo.
        }catch (FileNotFoundException fnfe){
        
            JOptionPane.showMessageDialog(null, "\"" + caminhoDoArquivo + "\" não existe.");
        
        // Caso o java não consiga fazer a operação de leitura do arquivo 'senhas.txt',
        // gera uma exceção que é tratada abaixo.
        }catch (IOException ioe){

            JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo.");
        
        }finally{ // Executado independente das cláusulas anteriores.
            
            try{
                
                br.close(); // Fechando o BufferedReader
                fr.close(); // Fechando o FileReader
            
            }catch (IOException iex) {
                
                JOptionPane.showMessageDialog(null, "Erro ao fechar o arquivo.");
                
            }
            
        }
        
        return -1;
        
    }
    
}
