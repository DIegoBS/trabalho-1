package modelo.dominio;

/**
 *
 * @author Jefferson
 */
public class No {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    public Usuario objeto;
    public No referencia;
    

    /*-----------------------------------
            CONSTRUTORES DA CLASSE
      -----------------------------------*/
    
    public No(){
        
    }
    
    public No(int c, String n) {
        
        this.setObjeto(new Usuario(c,n));
        this.setProximo(null);
        
    }
    
    /*-----------------------------------
           MÉTODOS get E set DA CLASSE
      -----------------------------------*/
    
    public void setProximo(No _r) {
        this.referencia = _r;
    } 

    public No getProximo() {
        return this.referencia;
    }

    public void setObjeto(Usuario _u){
        this.objeto = _u;
    }
    
    public Usuario getObjeto() {
        return this.objeto;
    }

    public Object getPosicao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
