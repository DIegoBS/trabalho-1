package modelo.dominio;

/**
 *
 * @author Jefferson
 */
public class Usuario {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    public int codigoUsuario;
    public String nomeUsuario;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    public Usuario(){
       
        
    }
    
    public Usuario(int _codigoUsuario, String _nomeUsuario) {
        this.codigoUsuario = _codigoUsuario;
        this.nomeUsuario = _nomeUsuario;
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    public int getCodigoUsuario() {
        return this.codigoUsuario;
    }

    public void setCodigoUsuario(int _codigoUsuario) {
        this.codigoUsuario = _codigoUsuario;
    }

    public String getNomeUsuario() {
        return this.nomeUsuario;
    }

    public void setNomeUsuario(String _nomeUsuario) {
        this.nomeUsuario = _nomeUsuario;
    }
    
}
